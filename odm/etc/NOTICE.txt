Notices for files contained in the odm filesystem image in this directory:
============================================================
Notices for file(s):
/odm/lib64/libopencv_core
------------------------------------------------------------
Software: opencv 4.5.5

Copyright notice:
Copyright (C) 2000-2021, Intel Corporation, all rights reserved.
Copyright (C) 2009-2011, Willow Garage Inc., all rights reserved.
Copyright (C) 2009-2016, NVIDIA Corporation, all rights reserved.
Copyright (C) 2010-2013, Advanced Micro Devices, Inc., all rights reserved.
Copyright (C) 2015-2021, OpenCV Foundation, all rights reserved.
Copyright (C) 2008-2016, Itseez Inc., all rights reserved.
Copyright (C) 2019-2021, Xperience AI, all rights reserved.
Copyright (C) 2019-2021, Shenzhen Institute of Artificial Intelligence and Robotics for Society, all rights reserved.
Copyright (C) 2016, NVIDIA Corporation, all rights reserved.
Copyright (C) 2015, NVIDIA Corporation, all rights reserved.
Copyright (C) 2014-2015, NVIDIA Corporation, all rights reserved.
Copyright (C) 2014, NVIDIA Corporation, all rights reserved.
Copyright (C) 2012-2015, NVIDIA Corporation, all rights reserved.
Copyright (C) 2012-2014, NVIDIA Corporation, all rights reserved.
Copyright (C) 2014-2016, NVIDIA Corporation, all rights reserved.
Copyright (C) 2013-2015, NVIDIA Corporation, all rights reserved.
Copyright (C) 2010 The Android Open Source Project
Copyright (c) 2008 - 2012 The Khronos Group Inc.
Copyright (c) 2008-2013 The Khronos Group Inc.
Copyright (c) 2008-2012 The Khronos Group Inc.
Copyright (c) 2008-2009 The Khronos Group Inc.
Copyright (c) 2008-2010 The Khronos Group Inc.
Copyright (c) 2008-2020 The Khronos Group Inc.
Copyright (c) 2014-2017 The Khronos Group Inc.
Christopher L Cheney <ccheney@debian.org>
Roland Stigge <stigge@antcom.de>

ichael Adams <mdadams@ece.uvic.ca>
Copyright (c) 1999-2000 Image Power, Inc.
Copyright (c) 1999-2000 The University of British Columbia
Copyright (c) 2001-2003 Michael David Adams
Copyright (c) 2002-2003 Michael David Adams.
Copyright (c) 2001-2006 Michael David Adams
Copyright (c) 2001-2002 Michael David Adams.
Copyright (c) 1999-2000, Image Power, Inc. and the University of
Copyright (c) 1999-2000 Image Power, Inc. and the University of
Copyright (c) 2001-2003 Michael David Adams.
Copyright (c) 2004 Michael David Adams.
Andrey Kiselev <dron@remotesensing.org>
Copyright (c) 2001-2004 Michael David Adams.
Copyright (C) 1994-1998, Thomas G. Lane.
Copyright (C) 1994-1996, Thomas G. Lane.
Copyright (C) 1994-1997, Thomas G. Lane.
Copyright (C) 1991-1996, Thomas G. Lane.
Copyright (C) 1991-1997, Thomas G. Lane.
Copyright (C) 1991-1998, Thomas G. Lane.
Copyright (C) 1995-1998, Thomas G. Lane.
Copyright (C) 1995-1997, Thomas G. Lane.
Copyright (C) 1991-1994, Thomas G. Lane.
Copyright (C) 1992-1996, Thomas G. Lane.
Copyright (C) 1992-1997, Thomas G. Lane.
Copyright (C) 1991-2020, Thomas G. Lane, Guido Vollbeding.
Copyright (C) 2015, 2018, D. R. Commander.
Copyright (C) 2009-2012, 2015, D. R. Commander.
Copyright 2009 Pierre Ossman <ossman@cendio.se> for Cendio AB
Copyright (C) 2014, MIPS Technologies, Inc., California.
Copyright (C) 1999-2006, MIYASAKA Masaru.
Copyright (C) 2011, 2014-2015, D. R. Commander.
Copyright (C) 2009-2011, 2014-2016, 2018-2019, D. R. Commander.
Copyright (C) 2015, Matthieu Darbois.
Copyright (C) 1997-1998, Thomas G. Lane, Todd Newman.
Copyright (C) 2017, D. R. Commander.
Copyright (C) 2020, D. R. Commander.
Copyright (C) 2010, D. R. Commander.
Copyright (C) 2010, 2016, 2018, D. R. Commander.
Copyright (C) 2009-2011, 2018, D. R. Commander.
Copyright (C) 2011, 2015, 2018, D. R. Commander.
Copyright (C) 2016, 2018, Matthieu Darbois.
Copyright (C) 2015, D. R. Commander.
Copyright (C) 2016, D. R. Commander.
Copyright (C) 2010, 2015-2018, 2020, D. R. Commander.
Copyright (C) 2015, Google, Inc.
Copyright (C) 2015-2018, D. R. Commander.
Copyright (C) 2013, 2016, D. R. Commander.
Copyright (C) 2010, 2015-2016, D. R. Commander.
Copyright (C) 2015, 2020, Google, Inc.
Copyright (C) 2013, Linaro Limited.
Copyright (C) 2014-2015, D. R. Commander.
Copyright (C) 2009, 2011, 2015, D. R. Commander.
Copyright (C) 2009, 2011-2012, 2014-2015, D. R. Commander.
Copyright (C) 2010, 2015, D. R. Commander.
Copyright (C) 2013, MIPS Technologies, Inc., California.
Copyright (C) 2009-2011, 2016, 2018-2019, D. R. Commander.
Copyright (C) 2010-2011, 2015-2016, D. R. Commander.
Copyright (C) 2010, 2016, D. R. Commander.
Copyright (C) 2012, 2015, D. R. Commander.
Copyright (C) 2009-2011, 2016, D. R. Commander.
Copyright (C) 1991-1995, Thomas G. Lane.
Copyright (C) 2009, 2011, 2014-2015, 2020, D. R. Commander.
Copyright (C) 2014-2015, 2018, 2020, D. R. Commander.
Copyright (C) 2011, 2015, 2020, D. R. Commander.
Copyright (C) 2015-2016, 2018, D. R. Commander.
Copyright (C) 2019, Arm Limited.
Copyright (C) 2014, 2017, D. R. Commander.
Copyright (C) 2015, 2020, D. R. Commander.
Copyright (C) 2014, D. R. Commander.
Copyright (C) 2017-2018, D. R. Commander.
Copyright (C) 2009, 2011, 2014-2015, 2018, 2020, D. R. Commander.
Copyright (C) 2010, 2020, D. R. Commander.
Copyright (C) 2015-2016, D. R. Commander.
Copyright (C) 2009-2011, 2013-2014, 2016-2017, 2020, D. R. Commander.
Copyright (C) 2009, 2015, D. R. Commander.
Copyright (C) 2009, 2014-2015, 2020, D. R. Commander.
Copyright (C) 2011, 2014, D. R. Commander.
Copyright (C) 2015-2016, 2018, Matthieu Darbois.
Copyright (C) 2009-2011, 2014, D. R. Commander.
Copyright (C) 2013, D. R. Commander.
Copyright (C) 1991-2012, Thomas G. Lane, Guido Vollbeding.
Copyright (C) 2010, 2012-2020, D. R. Commander.
Copyright (c) 2018-2019 Cosmin Truta
Copyright (c) 1998-2002,2004,2006-2018 Glenn Randers-Pehrson
Copyright (c) 1996-1997 Andreas Dilger
Copyright (c) 1995-1996 Guy Eric Schalnat, Group 42, Inc.
Copyright (c) 1995-2019 The PNG Reference Library Authors.
Copyright (c) 2018-2019 Cosmin Truta.
Copyright (c) 2000-2002, 2004, 2006-2018 Glenn Randers-Pehrson.
Copyright (c) 1996-1997 Andreas Dilger.
Copyright (c) 2000-2002, 2004, 2006-2018 Glenn Randers-Pehrson, are
Simon-Pierre Cadieux
Copyright (c) 1998-2000 Glenn Randers-Pehrson, are derived from
Tom Lane
Copyright (c) 1996-1997 Andreas Dilger, are derived from libpng-0.88, and are distributed according to the same disclaimer and license as Contributing John Bowler
Copyright (c) 1998-2002,2004,2006-2016,2018 Glenn Randers-Pehrson
Copyright (c) 2018 Cosmin Truta
Copyright (c) 1998-2002,2004,2006-2013 Glenn Randers-Pehrson
Copyright (c) 1998-2002,2004,2006-2017 Glenn Randers-Pehrson
Copyright (c) 1998-2002,2004,2006-2013,2018 Glenn Randers-Pehrson
Copyright (c) 2018-2019 Cosmin Truta */
Copyright (c) 1998-2002,2004,2006-2018 Glenn Randers-Pehrson */
Copyright (c) 1998-2002,2004,2006-2014,2016 Glenn Randers-Pehrson
Copyright (c) 1998-2018 Glenn Randers-Pehrson
Copyright (c) 1998-2002,2004,2006-2014,2016,2018 Glenn Randers-Pehrson
Copyright (c) 2014,2016 Glenn Randers-Pehrson

ans Rullgard, 2011.
James Yu <james.yu at linaro.org>, October 2013.

ans Rullgard, 2011.
Copyright (c) 2017-2018 Arm Holdings. All rights reserved.
Richard Townsend <Richard.Townsend@arm.com>, February 2017.
Copyright (c) 2016-2017 Glenn Randers-Pehrson

ike Klein and Matt Sarett

ike Klein and Matt Sarett, Google, Inc.
Copyright (c) 2016 Glenn Randers-Pehrson

andar Sahastrabuddhe, August 2016.

andar Sahastrabuddhe, 2016.
Copyright (c) 2017 Glenn Randers-Pehrson
Vadim Barkov, 2017.
Copyright (c) 1988-1997 Sam Leffler
Copyright (c) 1991-1997 Silicon Graphics, Inc.
Copyright (c) 2009 Frank Warmerdam
Copyright (c) 1991-1997 Sam Leffler
Copyright (c) 1990-1997 Sam Leffler
Lee Howard <faxguy@deanox.com>
Copyright (c) 1994-1997 Sam Leffler
Copyright (c) 1994-1997 Silicon Graphics, Inc.
Copyright (c) 1997 Greg Ward Larson
Copyright (c) 1997 Silicon Graphics, Inc.
Copyright (c) 2010, Andrey Kiselev <dron@ak4719.spb.edu>
Copyright (c) 1996-1997 Sam Leffler
Copyright (c) 1996 Pixar
Copyright (c) 1995-1997 Sam Leffler
Copyright (c) 1995-1997 Silicon Graphics, Inc.
Copyright (c) 1992-1997 Sam Leffler
Copyright (c) 1992-1997 Silicon Graphics, Inc.
Copyright (c) 2018, Mapbox
<norman.barker at mapbox.com>
Scott Wagner (wagner@itek.com), Itek Graphix, Rochester, NY USA
Copyright (c) 2017, Planet Labs
<even.rouault at spatialys.com>
Copyright (c) 2010, Google Inc. All rights reserved.
Copyright 2013 Google Inc. All Rights Reserved.
Urvang (urvang@google.com)
Copyright 2011 Google Inc. All Rights Reserved.
Skal (pascal.massimino@gmail.com)
Copyright 2015 Google Inc. All Rights Reserved.
Copyright 2010 Google Inc. All Rights Reserved.
somnath@google.com (Somnath Banerjee)
Copyright 2012 Google Inc. All Rights Reserved.
Vikas Arora(vikaas.arora@gmail.com)
Vikas Arora (vikaas.arora@gmail.com)
Jyrki Alakuijala (jyrki@google.com)
Copyright 2014 Google Inc. All Rights Reserved.
Branimir Vasic (branimir.vasic@imgtec.com)
Djordje Pesut  (djordje.pesut@imgtec.com)
Copyright 2017 Google Inc. All Rights Reserved.
Copyright 2016 Google Inc. All Rights Reserved.
Vincent Rabaud (vrabaud@google.com)
Djordje Pesut (djordje.pesut@imgtec.com)
Copyright 2018 Google Inc. All Rights Reserved.
Christian Duvivier (cduvivier@google.com)
Djordje Pesut    (djordje.pesut@imgtec.com)
Jovan Zelincevic (jovan.zelincevic@imgtec.com)
Prashant Patil   (prashant.patil@imgtec.com)
Somnath Banerjee (somnath@google.com)
Johann Koenig (johannkoenig@google.com)
cduvivier@google.com (Christian Duvivier)
Djordje Pesut    (djordje.pesut@imgtec.com)
Slobodan Prijic  (slobodan.prijic@imgtec.com)
Darko Laus (darko.laus@imgtec.com)

irko Raus (mirko.raus@imgtec.com)
Prashant Patil   (prashant.patil@imgtec.com)
Djordje Pesut (djordje.pesut@imgtec.com)
Prashant Patil (prashant.patil@imgtec.com)
Urvang Joshi (urvang@google.com)
Vincent Rabaud (vrabaud@google.com)
Prashant Patil (Prashant.Patil@imgtec.com)
Djordje Pesut (djordje.pesut@imgtec.com)
mans@mansr.com (Mans Rullgard)
somnath@google.com (Somnath Banerjee)
Branimir Vasic (branimir.vasic@imgtec.com)
Jyrki Alakuijala (jyrki@google.com)
Converted to C by Aleksander Kramarz (akramarz@google.com)
Vikas Arora (vikaas.arora@gmail.com)
Hui Su (huisu@google.com)
Urvang (urvang@google.com)
Vikas (vikasa@google.com)
Vikas Arora (vikaas.arora@gmail.com)
Jyrki Alakuijala (jyrki@google.com)
Urvang Joshi (urvang@google.com)
Vikas Arora (vikasa@google.com)
Skal (pascal.massimino@gmail.com)
Urvang (urvang@google.com)
Florian Kainz <kainz@ilm.com>
Rod Bogart <rgb@ilm.com>
Drew Hess <dhess@ilm.com>
Bill Anderson <wja@ilm.com>
Wojciech Jarosz <wjarosz@ucsd.edu>
Nick Porcino <NPorcino@lucasarts.com>
Paul Schneider <paultschneider@mac.com>
Andrew Kunz <akunz@ilm.com>
Piotr Stanczyk <pstanczyk@ilm.com>
Peter Hillman <peterh@weta.co.nz>
Nick Porcino <nick.porcino@gmail.com>
Simon Green <SGreen@nvidia.com>
Rito Trevino <etrevino@ilm.com>
Rodrigo Damazio <rdamazio@lsi.usp.br>
Greg Ward <gward@lmi.net>
Joseph Goldstone <joseph@lp.com>
Nicholas Yue <yue.nicholas@gmail.com>
Karl Rasche, DreamWorks Animation <Karl.Rasche@dreamworks.com>
Dustin Graves <dgraves@computer.org>
Jukka Liimatta <jukka.liimatta@twilight3d.com>
Baumann Konstantin <Konstantin.Baumann@hpi.uni-potsdam.de>
Daniel Koch <daniel@eyeonline.com>
E. Scott Larsen <larsene@cs.unc.edu>
stephan mantler <step@acm.org>
Andreas Kahler <AKahler@nxn-software.com>
Frank Jargstorff <fjargstorff@nvidia.com>
Copyright (c) 2002, Industrial Light & Magic, a division of Lucas
Primary Florian Kainz <kainz@ilm.com>
Copyright (c) 2008, Industrial Light & Magic, a division of Lucas
Copyright (c) 2002-2012, Industrial Light & Magic, a division of Lucas
Copyright (c) 2012, Industrial Light & Magic, a division of Lucas
Copyright (c) 2002-2018, Industrial Light & Magic, a division of Lucas
Copyright (c) 2009-2014 DreamWorks Animation LLC.
Copyright (c) 2007, Industrial Light & Magic, a division of Lucas
Copyright (c) 2004, Industrial Light & Magic, a division of Lucas
Copyright (c) 2006, Industrial Light & Magic, a division of Lucas
Copyright (c) 2009, Industrial Light & Magic, a division of Lucas
Copyright (c) 2003, Industrial Light & Magic, a division of Lucas
Copyright (c) 2012, Weta Digital Ltd
Copyright (c) 2011, Industrial Light & Magic, a division of Lucas
Copyright (c) 2013, Industrial Light & Magic, a division of Lucas
Copyright (c) 2007, Weta Digital Ltd
Portions (c) 2012 Weta Digital Ltd
Copyright (c) 2012, Autodesk, Inc.
Copyright (c) 2004, Pixar Animation Studios
Copyright (c) 2004, Industrial Light & Magic, a division of Lucasfilm
Entertainment Company Ltd.  Portions contributed and copyright held by others as indicated.  All rights reserved.
Copyright (c) 2005, Industrial Light & Magic, a division of Lucas and decoding routines written by Christian Rouet for his
Copyright (c) 2005-2012, Industrial Light & Magic, a division of Lucas
Copyright (c) 2004-2012, Industrial Light & Magic, a division of Lucas
Copyright (c) 2011-2012, Industrial Light & Magic, a division of Lucas
Copyright (c) 2006-2012, Industrial Light & Magic, a division of Lucas
Copyright (c) 2002-2014, Universite catholique de Louvain (UCL), Belgium
Copyright (c) 2002-2014, Professor Benoit Macq
Copyright (c) 2001-2003, David Janssens
Copyright (c) 2002-2003, Yannick Verschueren
Copyright (c) 2003-2007, Francois-Olivier Devaux
Copyright (c) 2003-2014, Antonin Descampe
Copyright (c) 2005, Herve Drolon, FreeImage Team
Copyright (c) 2008, 2011-2012, Centre National d'Etudes Spatiales (CNES), FR
Copyright (c) 2012, CS Systemes d'Information, France
Copyright (c) 2007, Jonathan Ballard <dzonatas@dzonux.net>
Copyright (c) 2007, Callum Lerwick <seg@haxxed.com>
Copyright (c) 2017, IntoPIX SA <support@intopix.com>
Copyright (c) 2008, Jerome Fimes, Communications & Systemes <jerome.fimes@c-s.fr>
Copyright (c) 2006-2007, Parvatha Elangovan
Copyright (c) 2010-2011, Kaori Hagihara
Copyright (c) 2011-2012, Centre National d'Etudes Spatiales (CNES), France
Copyright (c) 2012, Mathieu Malaterre <mathieu.malaterre@gmail.com>
Copyright (c) 2015, Mathieu Malaterre <mathieu.malaterre@gmail.com>
Copyright (c) 2015, Matthieu Darbois
Copyright (c) 2017, IntoPix SA <contact@intopix.com>
Copyright (c) 2012, Carl Hetherington
Copyright (c) 2016, Even Rouault
Copyright (C) 2016, Intel Corporation, all rights reserved.
Copyright 2008 Google Inc.  All rights reserved.
kenton@google.com (Kenton Varda)
jasonh@google.com (Jason Hsueh)
wink@google.com (Wink Saville), kenton@google.com (Kenton Varda)
jschorr@google.com (Joseph Schorr)
atenasio@google.com (Chris Atenasio) (ZigZag transform)
wink@google.com (Wink Saville) (refactored from wire_format.h)
brianolson@google.com (Brian Olson)
Copyright 2012 Google Inc.  All rights reserved.
Copyright 2013 Red Hat Inc.  All rights reserved.
Copyright 2014 Bloomberg Finance LP. All rights reserved.
Copyright 2015 Google Inc.  All rights reserved.
ogabbay@advaoptical.com (Oded Gabbay)
bsilver16384@gmail.com (Brian Silverman)
Copyright 2014 Google Inc. All rights reserved.
Copyright 2013 Google Inc.  All rights reserved.
Copyright 2014 Google Inc.  All rights reserved.
kenton@google.com (Kenton Varda) and others
laszlocsomor@google.com (Laszlo Csomor)
Anton Carver

axim Lifantsev
Copyright (c) 2006, Google Inc.
Copyright 2005-2008 Google Inc. All Rights Reserved.
jrm@google.com (Jim Meehan)
Copyright 2005 Google Inc.
lar@google.com (Laramie Leavitt)

att Austern
kenton@google.com (Kenton Varda)
ksroka@google.com (Krzysztof Sroka)
Copyright (C) 2010-2012 Daniel Beer <dlbeer@gmail.com>
Copyright (C) 1995-2011, 2016 Mark Adler
Copyright (C) 1995-2005, 2014, 2016 Jean-loup Gailly, Mark Adler
Copyright (C) 1995-2006, 2010, 2011, 2012, 2016 Mark Adler
Thanks to Rodney Brown <rbrown64@csc.com.au> for his contribution of faster
Copyright (C) 1995-2017 Jean-loup Gailly and Mark Adler
Leonid Broukhis.
Copyright (C) 1995-2016 Jean-loup Gailly
Copyright (C) 2004, 2010 Mark Adler
Copyright (C) 2004, 2005, 2010, 2011, 2012, 2013, 2016 Mark Adler
Copyright (C) 2004-2017 Mark Adler
Copyright (C) 1995-2016 Mark Adler
Copyright (C) 1995-2017 Mark Adler
Copyright (C) 1995-2003, 2010 Mark Adler
Copyright (C) 1995-2005, 2010 Mark Adler
Copyright (C) 1995-2017 Jean-loup Gailly
Copyright (C) 1995-2003, 2010, 2014, 2016 Jean-loup Gailly, Mark Adler
Copyright (C) 1995-2016 Jean-loup Gailly, Mark Adler
Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
Copyright (C) 2009, Willow Garage Inc., all rights reserved.
Copyright (C) 2013, OpenCV Foundation, all rights reserved.
Copyright (C) 2000, Intel Corporation, all rights reserved.
Copyright (C) 2018 Intel Corporation
Copyright (C) 2000-2015, Intel Corporation, all rights reserved.
Copyright (C) 2015, OpenCV Foundation, all rights reserved.
Copyright (C) 2015, Itseez Inc., all rights reserved.
Copyright (C) 2014, Itseez Inc, all rights reserved.
Copyright (C) 2009-2010, Willow Garage Inc., all rights reserved.
Copyright (C) 2008-2012, Willow Garage Inc., all rights reserved.
Copyright (C) 2014, Itseez Inc., all rights reserved.
Copyright (C) 2014, Advanced Micro Devices, Inc., all rights reserved.
Copyright (C) 2020, Huawei Technologies Co., Ltd. All rights reserved.
Liangqian Kong <kongliangqian@huawei.com>
Longbu Wang <wanglongbu@huawei.com>
Liangqian Kong <chargerKong@126.com>
Longbu Wang <riskiest@gmail.com>
Copyright (C) 2015, Itseez, Inc., all rights reserved.
Copyright (c) 2013 NVIDIA Corporation. All rights reserved.
Copyright (C) 2020, Institute of Software, Chinese Academy of Sciences.
Copyright (C) 2018-2019, Intel Corporation, all rights reserved.
Copyright 2008-2009  Marius Muja (mariusm@cs.ubc.ca). All rights reserved.
Copyright 2008-2009  David G. Lowe (lowe@cs.ubc.ca). All rights reserved.
Copyright 2008-2011  Marius Muja (mariusm@cs.ubc.ca). All rights reserved.
Copyright 2008-2011  David G. Lowe (lowe@cs.ubc.ca). All rights reserved.
Vincent Rabaud
Copyright (C) 2018-2020 Intel Corporation
Copyright (C) 2019 Intel Corporation
Copyright (C) 2018-2019 Intel Corporation
Copyright (C) 2020 Intel Corporation
Copyright (C) 2018-2021 Intel Corporation
Copyright (C) 2019-2020 Intel Corporation
Copyright (C) 2019-2021 Intel Corporation
Copyright (C) 2020-2021 Intel Corporation
Copyright (C) 2021 Intel Corporation
Copyright 2012. All rights reserved.
rootDownloadsopencv-4.5.2build-ubuntu-sharedinstallsharelicensesopencv4libtiff-COPYRIGHT
Copyright 2014-2019 Intel Corporation All Rights Reserved.
copyright laws and treaty provisions. No part of the Material may be used, copied, reproduced, modified, published, uploaded, posted, transmitted,distributed or disclosed in any way without Intel's prior express written permission. No license under any patent, copyright or other intellectual
Copyright 1999-2019 Intel Corporation All Rights Reserved.
Copyright 2015-2019 Intel Corporation All Rights Reserved.
Copyright 2001-2019 Intel Corporation All Rights Reserved.
Copyright 2016-2019 Intel Corporation.
Copyright (c) 2005-2020 Intel Corporation
Copyright (c) 2011, Google Inc.
Copyright (C) 2015-2016, OpenCV Foundation, all rights reserved.
Copyright (C) 2010-2012, Multicoreware, Inc., all rights reserved.
Copyright (C) 2010-2012, Advanced Micro Devices, Inc., all rights reserved.
Copyright (c) 2013, Bo Li (prclibo@gmail.com), ETH Zurich
Copyright (C) 2014, Samson Yilma (samson_yilma@yahoo.com), all rights reserved.
Copyright (C) 2018, Intel Corporation, all rights reserved.
Copyright (C) 2009, Intel Corporation and others, all rights reserved.
Copyright (C) 2019 Czech Technical University.
Daniel Barath (barath.daniel@sztaki.mta.hu)

odification: Maksym Ivashechkin (ivashmak@cmp.felk.cvut.cz)
Copyright (C) 2008-2013, Willow Garage Inc., all rights reserved.
Copyright (C) 2016, OpenCV Foundation, all rights reserved.
Copyright (C) 2010-2012, Institute Of Software Chinese Academy Of Science, all rights reserved.
Fangfang Bai, fangfang@multicorewareinc.com
Jin Ma,       jin@multicorewareinc.com
Copyright (C) 2014-2015, Itseez Inc., all rights reserved.
Copyright 2015-2017 Philippe Tillet
Copyright (c) 2017, Intel Corporation
Copyright (C) 2000-2018, Intel Corporation, all rights reserved.
Copyright (c) 2011. Philipp Wagner <bytefish[at]gmx[dot]de>.
Copyright (C) 2013, NVIDIA Corporation, all rights reserved.
Jia Haipeng, jiahaipeng95@gmail.com
Peng Xiao, pengxiao@multicorewareinc.com
Copyright (C) 2014, Itseez, Inc., all rights reserved.
abratchik
Copyright (C) 2017, Intel Corporation, all rights reserved.
Copyright (C) 2020, Intel Corporation, all rights reserved.
COPYRIGHT
Copyright (c) 2014, The Regents of the University of California (Regents)
Copyright (c) 2014, the respective contributors
Copyright (c) 2017 Joseph Redmon
Copyright (c) 2016-2017 Fabian David Tschopp, all rights reserved.
Copyright (c) 2020, OPEN AI LAB
qtang@openailab.com
Copyright (C) 2017-2019, Intel Corporation, all rights reserved.
Copyright (C) 2013, Evgeny Toropov, all rights reserved.
Copyright (C) 2008, Willow Garage Inc., all rights reserved.
Copyright (C) 2011  The Autonomous Systems Lab (ASL), ETH Zurich, Stefan Leutenegger, Simon Lynen and Margarita Chli.
Copyright (c) 2009, Willow Garage, Inc.
Copyright (C) 2009, Liu Liu All rights reserved.
Ethan Rublee, Vincent Rabaud, Gary Bradski
Copyright (c) 2006-2010, Rob Hess <hess@eecs.oregonstate.edu>
David Lowe (lowe@cs.ubc.ca)
Pablo F. Alcantarilla (1), Jesus Nuevo (2)
Email: pablofdezalc@gmail.com
AKAZE Features Copyright 2013, Pablo F. Alcantarilla, Jesus Nuevo
Pablo F. Alcantarilla
KAZE Features Copyright 2012, Pablo F. Alcantarilla
Niko Li, newlife20080214@gmail.com
Zero Lin, Zero.Lin@amd.com
Zhang Ying, zhangying913@gmail.com
Yao Wang, bitwangyaoyao@gmail.com
Copyright (C) 2018 - 2020 Intel Corporation
Copyright (c) Microsoft Open Technologies, Inc.
Copyright (C) 2008-2010, Willow Garage Inc., all rights reserved.
Copyright (C) 2020, Stefan Brüns <stefan.bruens@rwth-aachen.de>
Copyright (C) 2000-2016, Intel Corporation, all rights reserved.
Copyright (C) 2015-2016, Itseez Inc., all rights reserved.
<patcherwork@gmail.com>
Nathan, liujun@multicorewareinc.com
Copyright (C) 2000-2008, 2018, Intel Corporation, all rights reserved.
2021 Federico Bolelli <federico.bolelli@unimore.it>
2021 Stefano Allegretti <stefano.allegretti@unimore.it>
2021 Costantino Grana <costantino.grana@unimore.it>
2011 Jason Newton <nevion@gmail.com>
2016, 2021 Costantino Grana <costantino.grana@unimore.it>
2016, 2021 Federico Bolelli <federico.bolelli@unimore.it>
2016 Lorenzo Baraldi <lorenzo.baraldi@unimore.it>
2016 Roberto Vezzani <roberto.vezzani@unimore.it>
2016 Michele Cancilla <cancilla.michele@gmail.com>
Copyright (C) 2014, Itseez, Inc, all rights reserved.
Copyright (C) 2009-2012, Willow Garage Inc., all rights reserved.
Copyright (C) 2008-2011, Willow Garage Inc., all rights reserved.
Nghia Ho, nghiaho12@yahoo.com
Ovidiu Parvu
Copyright (C) 2013, Ovidiu Parvu, all rights reserved.
Copyright (c) 2008-2011, William Lucas
Copyright (C) 2000-2008, 2017, Intel Corporation, all rights reserved.
Copyright (C) 2019-2020, Intel Corporation, all rights reserved.
Copyright (C) 2000-2020 Intel Corporation, all rights reserved.
Copyright (C) 2016, Itseez, Inc, all rights reserved.
Shengen Yan, yanshengen@gmail.com
Jiang Liyuan, lyuan001.good@163.com
Rock Li, Rock.Li@amd.com
Wu Zailong, bullet@yeah.net
Xu Pang, pangxu010@163.com
Sen Liu, swjtuls1987@126.com
Yao Wang yao@multicorewareinc.com
Sajjad Taheri, University of California, Irvine. sajjadt[at]uci[dot]edu
Copyright (c) 2015 The Regents of the University of California (Regents)
Author : Rijubrata Bhaumik, Intel Corporation. rijubrata.bhaumik[at]intel[dot]com
Copyright( C) 2000, Intel Corporation, all rights reserved.
Rahul Kavi rahulkavi[at]live[at]com
Copyright (C) 2016, Itseez Inc, all rights reserved.
Copyright (C) 2008-2013, Itseez Inc., all rights reserved.
Copyright (C) 2013, Itseez Inc, all rights reserved.
Jiang Liyuan,jlyuan001.good@163.com
Zailong Wu, bullet@yeah.net
Copyright (C) 2014, Intel, Inc., all rights reserved.
Copyright 2005, Google Inc.
registration from Barthelemy Dagenais' (barthelemy@prologique.com)
Copyright 2008, Google Inc.
Z.Zivkovic, www.zoranz.net
Copyright (c) 2015, Piotr Dobrowolski dobrypd[at]gmail[dot]com
Copyright (C) 2008, 2011, Nils Hasler, all rights reserved.
Nils Hasler <hasler@mpi-inf.mpg.de>
Dirk Van Haerenborgh <vhdirk@gmail.com>
Copyright (C) 2008, Nils Hasler, all rights reserved.
Nils Hasler <hasler@mpi-inf.mpg.de>
Konstantin Dols <dols@ient.rwth-aachen.de>

ark Asbach <asbach@ient.rwth-aachen.de>
Copyright (c) Microsoft. All rights reserved.
Siddharth Kherada <siddharthkherada27[at]gmail[dot]com>
Samyak Datta (datta[dot]samyak[at]gmail.com)
Original  Denis Burenkov
AMS and Direct Methods  Jasper Shemilt
Ethan Rublee
G. Evangelidis, INRIA, Grenoble, France
Steve Nicholson
Kevin Hughes <kevinhughes27[at]gmail[dot]com>
Copyright 2010 Argus Corp. All rights reserved.
Kevin Hughes
edgar
Edgar Riba
eriba
Dmitry Matveev, dmitry.matveev@intel.com, based on sample by Karpushin Vladislav, karpushin@ngs.ru
Karpushin Vladislav, karpushin@ngs.ru, https://github.com/VladKarpushin
Xing Sun <winfredsun@tencent.com>
Feng Zheng <zhengf@sustech.edu.cn>
Xinyang Jiang <sevjiang@tencent.com>
Fufu Yu <fufuyu@tencent.com>
Enwei Zhang <miyozhang@tencent.com>
Copyright (C) 2020-2021, Tencent.
Copyright (C) 2020-2021, SUSTech.
Copyright (c) 2007-2008 Intel Corporation. All Rights Reserved.
Copyright (c) Microsoft Corporation. All rights reserved
Copyright (C) Microsoft Corporation. All rights reserved.
Copyright (c) Microsoft Corporation. All rights reserved.
Copyright (c) Microsoft Corporation. All rights reserved.



License:Apache License V2.0, BSD 3-Clause License
Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
BSD 3-Clause License   
Copyright (c) <YEAR>, <OWNER>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

